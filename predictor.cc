#include "predictor.h"
#include <cstdint>

/////////////// STORAGE BUDGET JUSTIFICATION ////////////////

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

// Constructeur du prédicteur
PREDICTOR::PREDICTOR(char *prog, int argc, char *argv[])
{
   // La trace est tjs présente, et les arguments sont ceux que l'on désire
   if (argc != 2) {
      fprintf(stderr, "usage: %s <trace> pcbits countbits\n", prog);
      exit(-1);
   }

   uint32_t pcbits    = strtoul(argv[0], NULL, 0);
   uint32_t countbits = strtoul(argv[1], NULL, 0);

   nentries = (1 << pcbits);        // nombre d'entrées dans la table
   pcmask   = (nentries - 1);       // masque pour n'accéder qu'aux bits significatifs de PC
   countmax = (1 << countbits) - 1; // valeur max atteinte par le compteur à saturation
   table    = new uint32_t[nentries]();


   // gshare
   uint8_t ght_size = pcbits; // bits
   ght_mask = (1 << ght_size) - 1;
   global_history = 0;

}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

bool PREDICTOR::GetPrediction(UINT64 PC)
{
   // gshare
   uint32_t index = (PC ^ global_history) & pcmask;
   printf("GetPrediction: index = %d\n", index);
   uint32_t v = table[index];
   return v > countmax / 2 ? TAKEN : NOT_TAKEN;
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

void PREDICTOR::UpdatePredictor(UINT64 PC, OpType opType, bool resolveDir, bool predDir, UINT64 branchTarget)
{
   // gshare
   uint32_t index = (PC ^ global_history) & pcmask;
   // uint32_t index = (PC & pcmask) ^ global_history;
   global_history = ght_mask & ((global_history << 1) + (resolveDir == TAKEN));

   uint32_t sat_counter = table[index];
   table[index] = resolveDir == TAKEN ? SatIncrement(sat_counter, countmax) : SatDecrement(sat_counter);
   // table[index] = ght_mask & ((v << 1) + (resolveDir == TAKEN));// resolveDir == TAKEN ? SatIncrement(v, countmax) : SatDecrement(v);
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

void PREDICTOR::TrackOtherInst(UINT64 PC, OpType opType, bool branchDir, UINT64 branchTarget)
{
   // This function is called for instructions which are not
   // conditional branches, just in case someone decides to design
   // a predictor that uses information from such instructions.
   // We expect most contestants to leave this function untouched.
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////


/***********************************************************/
